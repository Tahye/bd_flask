from flask import Flask, render_template, request, url_for, flash,Response, make_response
from werkzeug.utils import redirect
from flask_mysqldb import MySQL
from flask import Flask, render_template, request, redirect, url_for, session,flash
from flask_mysqldb import MySQL
import MySQLdb.cursors
import re
from fpdf import FPDF
app = Flask(__name__)
app.secret_key = 'many random bytes'

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'hotel_g'

mysql = MySQL(app)
@app.route('/pythonlogin/home')
def home():
    if 'loggedin' in session:
        # User is loggedin show them the home page
        return render_template('home/home.html', username=session['username'],title="Home")
    # User is not loggedin redirect to login page
    return redirect(url_for('login'))
@app.route('/client')
def Client():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM client")
    data = cur.fetchall()
    cur.close()

    return render_template('client.html', clients=data)


@app.route('/insert', methods = ['POST'])
def insert():
    if request.method == "POST":
        flash("Le client insere avec succes")
        
        Nom = request.form['Nom']
        prenom = request.form['prenom']
        phone = request.form['phone']
        Email = request.form['Email']
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO client (Nom, prenom, phone,Email) VALUES (%s, %s, %s,%s)", (Nom, prenom, phone,Email))
        mysql.connection.commit()
        return redirect(url_for('Client'))

@app.route('/delete/<string:id_cl>', methods = ['GET'])
def delete(id_cl):
    flash("L'enregisement a ete supprime avec succes ")
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM client WHERE id_cl=%s", (id_cl,))
    mysql.connection.commit()
    return redirect(url_for('Client'))



@app.route('/update', methods= ['POST'])
def update():
    if request.method == 'POST':
        id_cl = request.form['id_cl']
        Nom = request.form['Nom']
        prenom = request.form['prenom']
        phone = request.form['phone']
        Email = request.form['Email']
        cur = mysql.connection.cursor()
        cur.execute("""
        UPDATE client SET Nom=%s,prenom=%s,phone=%s,Email=%s
        WHERE id_cl=%s
        """, (Nom, prenom, phone, Email, id_cl))
        mysql.connection.commit()
        flash("le client es modifier avec succees")
        return redirect(url_for('Client'))
    else:
        return "eo"

#ho

@app.route('/hotel')
def hotel():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM hotel")
    data = cur.fetchall()
    cur.close()

    return render_template('hotel.html', hotels=data)


@app.route('/insert_h', methods = ['POST'])
def insert_h():
    if request.method == "POST":
        flash("L'hotel insere avec succes")
        
        Nom_h = request.form['Nom_h']
        Adresse = request.form['Adresse']
        Ville = request.form['Ville']
        Pays = request.form['Pays']
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO hotel (Nom_h, Adresse, Ville,Pays) VALUES (%s, %s, %s,%s)", (Nom_h, Adresse, Ville,Pays))
        mysql.connection.commit()
        return redirect(url_for('hotel'))

@app.route('/delete_h/<string:id_h>', methods = ['GET'])
def delete_h(id_h):
    flash("L'enregisement a ete supprime avec succes ")
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM hotel WHERE id_h=%s", (id_h,))
    mysql.connection.commit()
    return redirect(url_for('hotel'))



@app.route('/update_h', methods= ['POST'])
def update_h():
    if request.method == 'POST':
        id_h = request.form['id_h']
        Nom_h = request.form['Nom_h']
        Adresse = request.form['Adresse']
        Ville = request.form['Ville']
        Pays = request.form['Pays']
        cur = mysql.connection.cursor()
        cur.execute("""
        UPDATE hotel SET Nom_h=%s,Adresse=%s,Ville=%s,Pays=%s
        WHERE id_h=%s
        """, (Nom_h, Adresse, Ville, Pays, id_h))
        mysql.connection.commit()
        flash("l'hotel es modifier avec succees")
        return redirect(url_for('hotel'))
    else:
        return "eo"

@app.route('/chambre')
def chambre():
    cur = mysql.connection.cursor()
    # Récupérer la liste des chambres depuis la base de données
    cur.execute("SELECT chambre.id_c, hotel.Nom_h,chambre.type, chambre.prix, chambre.Disponibilite FROM chambre INNER JOIN hotel ON chambre.id_h = hotel.id_h")
    chambres = cur.fetchall()

    # Récupérer la liste des hôtels depuis la base de données
    cur.execute("SELECT id_h, Nom_h FROM hotel")
    hotels = cur.fetchall()
    
    return render_template('chambre.html', chambres=chambres, hotels=hotels)



@app.route('/insert_c', methods = ['POST'])
def insert_c():
    if request.method == "POST":
        flash("Le chambre insere avec succes")
        
        id_h = request.form['id_h']
        Disponibilite= request.form['Disponibilite']
        type= request.form['type']

        prix = request.form['prix']
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO chambre (id_h,type,prix, Disponibilite) VALUES (%s,%s,%s,%s)", (id_h,type,prix,Disponibilite))
        mysql.connection.commit()
        return redirect(url_for('chambre'))
    
@app.route('/delete_c/<string:id_c>', methods = ['GET'])
def delete_c(id_c):
    flash("L'enregisement a ete supprime avec succes ")
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM chambre WHERE id_c=%s", (id_c,))
    mysql.connection.commit()
    return redirect(url_for('chambre'))



@app.route('/update_c', methods= ['POST'])
def update_c():
    if request.method == 'POST':
        id_c = request.form['id_c']
        id_h = request.form['id_h']
        Disponibilite= request.form['Disponibilite']
        prix = request.form['prix']
        type = request.form['type']

        cur = mysql.connection.cursor()
        cur.execute("""
        UPDATE chambre SET id_h=%s,type=%s,prix=%s,Disponibilite=%s
        WHERE id_c=%s
        """, (id_h,type, prix,Disponibilite,id_c))
        mysql.connection.commit()
        flash("le chambre es modifier avec succees")
        return redirect(url_for('chambre'))
    else:
        return "eo"

@app.route('/reservation')
def reservation():
    cur = mysql.connection.cursor()
    # Récupérer la liste des chambres depuis la base de données
    cur.execute("SELECT r.id_r,chambre.prix, client.Nom,r.DateDep,r.DateArr FROM chambre ,reservation r, client where chambre.id_c = r.id_c and client.id_cl=r.id_cl")
    reservations = cur.fetchall()

    # Récupérer la liste des hôtels depuis la base de données
    cur.execute("SELECT id_c, prix FROM chambre")
    chambres = cur.fetchall()
    cur.execute("SELECT id_cl, Nom FROM client")
    clients = cur.fetchall()
    return render_template('reservation.html', chambres=chambres, reservations=reservations,clients=clients)



@app.route('/insert_r', methods = ['POST'])
def insert_r():
    if request.method == "POST":
        flash("Le reservation insere avec succes")
        
        id_c = request.form['id_c']
        id_cl = request.form['id_cl']
        DateDep= request.form['DateDep']
        DateArr = request.form['DateArr']
              
       
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO reservation (id_c,DateDep,DateArr, id_cl) VALUES (%s,%s,%s,%s)", (id_c,DateDep,DateArr, id_cl))
        mysql.connection.commit()
        return redirect(url_for('reservation'))
    
@app.route('/delete_r/<string:id_r>', methods = ['GET'])
def delete_r(id_r):
    flash("L'enregisement a ete supprime avec succes ")
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM reservation WHERE id_r=%s", (id_r,))
    mysql.connection.commit()
    return redirect(url_for('reservation'))



@app.route('/update_r', methods= ['POST'])
def update_r():
    if request.method == 'POST':
        id_c = request.form['id_c']
        id_cl = request.form['id_cl']
        DateDep= request.form['DateDep']
        DateArr = request.form['DateArr']
              
        id_r = request.form['id_r']


        cur = mysql.connection.cursor()
        cur.execute("""
        UPDATE reservation SET id_c=%s,DateDep=%s,DateArr=%s,id_cl=%s
        WHERE id_r=%s
        """, (id_c,DateDep, DateArr,id_cl,id_r))
        mysql.connection.commit()
        flash("la reservation es modifier avec succees")
        return redirect(url_for('reservation'))
    else:
        return "eo"






@app.route('/', methods=['GET', 'POST'])
def login():

    if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
       
        username = request.form['username']
        password = request.form['password']
       
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM accounts WHERE username = %s AND password = %s', (username, password))
        
        account = cursor.fetchone()
             
        if account:
            
            session['loggedin'] = True
            session['id'] = account['id']
            session['username'] = account['username']
           
            return redirect(url_for('home'))
        else:
            
            flash(" Nom d'utilisateur/mot de passe incorrect", "danger")
    return render_template('auth/login.html',title="Login")




@app.route('/pythonlogin/register', methods=['GET', 'POST'])
def register():
    
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form and 'email' in request.form:
       
        username = request.form['username']
        password = request.form['password']
        email = request.form['email']
                
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        
        cursor.execute( "SELECT * FROM accounts WHERE username LIKE %s", [username] )
        account = cursor.fetchone()
        
        if account:
            flash("Account already exists!", "danger")
        elif not re.match(r'[^@]+@[^@]+\.[^@]+', email):
            flash("Invalid email address!", "danger")
        elif not re.match(r'[A-Za-z0-9]+', username):
            flash("Le nom d'utilisateur doit contenir uniquement des lettres et des chiffres ", "danger")
        elif not username or not password or not email:
            flash("Nom d'utilisateur/mot de passe incorrect ", "danger")
        else:
        
            cursor.execute('INSERT INTO accounts VALUES (NULL, %s, %s, %s)', (username,email, password))
            mysql.connection.commit()
            flash("Vous vous êtes inscrit(e) avec succès !", "success")
            return redirect(url_for('login'))

    elif request.method == 'POST':
       
        flash("Veuillez remplir le formulaire s'il vous plaît !", "danger")
    
    return render_template('auth/register.html',title="Register")



    



@app.route('/pythonlogin/logout')
def logout():
    
   session.pop('loggedin', None)
   session.pop('id', None)
   session.pop('username', None)
   
   return redirect(url_for('login'))
@app.route('/exportpdf')
def exportpdf():
        pdf = FPDF()
        pdf.add_page()
        pdf.set_font('Times', 'B', 16.0)
        page_width = pdf.w - 2 * pdf.l_margin
        
        cur = mysql.connection.cursor()
        
        cur.execute(
            "SELECT ca.prix,c.Nom,r.DateDep,r.DateArr FROM `reservation` r , client c ,chambre ca where(r.id_c=ca.id_c) and (r.id_cl=c.id_cl)")
        result = cur.fetchall()


        pdf.set_font('Times', '', 12)

        col_width = page_width / 4
        col_width0 = page_width / 6

        pdf.ln(1)

        th = pdf.font_size
        pdf.cell(col_width0, th, 'Prix', border=1)
        pdf.cell(col_width0, th, 'Client', border=1)
        pdf.cell(col_width, th, 'Date d\'entree', border=1)
        pdf.cell(col_width, th, 'Date de Sortie', border=1)
        pdf.ln(th)
        print(result)
        for row in result:
            pdf.cell(col_width0, th, str(row[0]), border=1)
            pdf.cell(col_width0, th, row[1], border=1)
            pdf.cell(col_width, th, str(row[2]), border=1)
            pdf.cell(col_width, th, str(row[3]), border=1)
            
            pdf.ln(th)
        sem='reservation'
        pdf.ln(10)
        return Response(pdf.output(dest='S').encode('latin-1'), mimetype='application/pdf',headers={'Content-Disposition': 'attachment;filename= '+sem+'.pdf'})


if __name__ == "__main__":
    app.run(debug=True)
